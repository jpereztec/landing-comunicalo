$(document).ready(function(){
	$('.contacto').tecnoMailer({
		validateOptions: {
			rules: {
				'p[nombre]': {
                    lettersonly: true,
                    required: true,
                },
                'p[email]': {
                    email: true,
                },
                'p[telefono]': {
                    phoneMX: true,
                    required: true,
                    maxlength: 10,
                    minlength: 10,
                },
                'p[mensaje]': {
                    required: true,
                },
			}
		}
	});
});